# 每日bing壁纸

#### 项目介绍
自动获取每天最新bing壁纸，可将其用作网站背景背景使用。

#### 具体用法


```
body {
  background: url(接口地址) no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
```



#### 演示地址（图片每天自动更换）：

http://demo.hezi.moqingwu.com/bing/bing.php



#### 版本说明

直接显示版（bing.php）：每天自动获取bing壁纸

自动下载版（bing_download.php）：每天自动获取bing壁纸，并将图片资源保存到自己服务器。用法：设置云监控任务，让其每日自动访问该网站即可。


#### 更多源码

http://hezi.moqingwu.com

